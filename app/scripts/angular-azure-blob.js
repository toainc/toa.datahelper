angular.module("azureBlobStorage", []), angular.module("azureBlobStorage").factory("azureBlobUpload", ["$log", function ($log) {
    function upload(config) {
        function log(logType, message) {
            var logTypes = {
                debug: $log.debug,
                error: $log.error
            };
            logTypes[logType](message)
        }
        var worker = new window.Worker(config.path + (config.workerFileName || "azure-blob-upload-worker.js"));
        console.log(worker);
        $log.debug("worker");
        return worker.postMessage({
            type: "file",
            file: config.file
        }), worker.postMessage({
            type: "config",
            config: {
                blobUri: config.blobUri,
                blockSize: config.blockSize,
                calculateFileMd5: config.calculateFileMd5,
                libPath: config.libPath || config.path
            }
        }), worker.onmessage = function (e) {
            switch (e.data.type) {
                case "ready":
                    worker.postMessage({
                        type: "upload"
                    });
                    break;
                case "progress":
                    config.progress(e.data.payload);
                    break;
                case "complete":
                    config.complete(e.data.payload);
                    break;
                case "log":
                    log(e.data.logType, e.data.message);
                    break;
                case "error":
                    break;
                default:
                    throw new Error("Don't know what to do with message of type " + e.data.type)
            }
        }, {
            cancel: function () {
                worker.postMessage({
                    type: "cancel"
                })
            }
        }
    }
    return {
        upload: upload
    }
}]), angular.module("azureBlobStorage").directive("fileSelected", ["$parse", "$http", "$timeout", function ($parse, $http, $timeout) {
    return function (scope, elem, attr) {
        var fn = $parse(attr.ngFileSelect);
        elem.bind("change", function (evt) {
            var fileList, i, files = [];
            if (fileList = evt.target.files, null !== fileList)
                for (i = 0; i < fileList.length; i++) files.push(fileList.item(i));
            $timeout(function () {
                fn(scope, {
                    $files: files,
                    $event: evt
                })
            })
        }), elem.bind("click", function () {
            this.value = null
        })
    }
}]);
