'use strict';

/**
 * @ngdoc overview
 * @name dbManApp
 * @description
 * # dbManApp
 *
 * Main module of the application.
 */
angular
    .module('dbManApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ng-file-input',
    'datetimepicker',
    'ngTouch',
    'azureBlobStorage'
  ])
    .config(['datetimepickerProvider', '$routeProvider', '$locationProvider', function (datetimepickerProvider, $routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main'
            })
            .when('/about', {
                templateUrl: 'views/about.html',
                controller: 'AboutCtrl',
                controllerAs: 'about'
            })
            .when('/eventos', {
                templateUrl: 'views/eventos.html',
                controller: 'EventosCtrl',
                controllerAs: 'eventos'
            })
            .when('/establecimientos', {
                templateUrl: 'views/establecimientos.html',
                controller: 'EstablecimientosCtrl',
                controllerAs: 'establecimientos'
            })
            .when('/comunidades', {
              templateUrl: 'views/comunidades.html',
              controller: 'ComunidadesCtrl',
              controllerAs: 'comunidades'
            })
            .otherwise({
                redirectTo: '/'
            });
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
        datetimepickerProvider.setOptions({
                            locale: 'en',
            widgetPositioning:{
            horizontal:'left',
                vertical: 'auto'
            },
            format: 'YYYY-MM-DD HH:mm'
                        });
  }]).run(['$location', function AppRun($location) {

}]);;

var url_base = "http://40.78.108.151:7474/db/data";
    function rotateCard(btn){
        var $card = $(btn).closest('.card-container');
        console.log($card);
        if($card.hasClass('hover')){
            $card.removeClass('hover');
        } else {
            $card.addClass('hover');
        }
    }
