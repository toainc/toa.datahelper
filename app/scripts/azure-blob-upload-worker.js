! function () {
    if ("performance" in self == !1 && (self.performance = {}), Date.now = Date.now || function () {
            return (new Date).getTime()
        }, "now" in self.performance == !1) {
        var nowOffset = Date.now();
        performance.timing && performance.timing.navigationStart && (nowOffset = performance.timing.navigationStart), self.performance.now = function () {
            return Date.now() - nowOffset
        }
    }
}(),
function () {
    function setFile(f) {
        file = f
    }

    function error(data, status) {
        self.postMessage({
            type: "error",
            data: data,
            status: status
        }), self.close()
    }

    function setConfig(config) {
        var defaultBlockSize = 1048576,
            blockSize = config.blockSize ? config.blockSize : defaultBlockSize,
            maxBlockSize = blockSize,
            numberOfBlocks = 1;
        file || $log.error("MUST set file before setting config");
        var fileSize = file.size;
        blockSize > fileSize && (maxBlockSize = fileSize, $log.debug("max block size = " + maxBlockSize)), numberOfBlocks = fileSize % maxBlockSize === 0 ? fileSize / maxBlockSize : parseInt(fileSize / maxBlockSize, 10) + 1, $log.debug("total blocks = " + numberOfBlocks), state = {
            libPath: config.libPath,
            maxBlockSize: maxBlockSize,
            numberOfBlocks: numberOfBlocks,
            totalBytesRemaining: fileSize,
            fileSize: fileSize,
            currentFilePointer: 0,
            blocks: [],
            blockIdPrefix: "block-",
            blocksReadIndex: 0,
            bytesUploaded: 0,
            file: file,
            blobUri: config.blobUri,
            error: config.error,
            cancelled: !1,
            calculateFileMd5: config.calculateFileMd5 || !1,
            fileMd5: arrayBufferUtils.getArrayBufferMd5Iterative(),
            readingNextSetOfBlocks: !1
        }
    }

    function readNextSetOfBlocks(config) {
        var numberOfBlocksToRead = 10,
            done = config.done || function () {},
            doneOne = config.doneOne || function () {},
            doneHalf = config.doneHalf || function () {},
            alreadyReading = config.alreadyReading || function () {
                $log.debug("Already reading next set of blocks!")
            };
        if (config.state.readingNextSetOfBlocks) return void alreadyReading();
        config.state.readingNextSetOfBlocks = !0;
        var fileReader = new FileReaderSync,
            skip = state.blocksReadIndex,
            numberOfBlocks = state.numberOfBlocks;
        if (skip >= numberOfBlocks) return config.state.readingNextSetOfBlocks = !1, void done();
        var end = skip + numberOfBlocksToRead > numberOfBlocks ? numberOfBlocks : skip + numberOfBlocksToRead,
            blocksToRead = config.state.blocks.slice(skip, end),
            currentIndex = 0,
            readNextBlock = function () {
                var fileContent = config.state.file.slice(blocksToRead[currentIndex].pointer, blocksToRead[currentIndex].end),
                    result = fileReader.readAsArrayBuffer(fileContent);
                loaded(result)
            },
            loaded = function (result) {
                var currentBlock = blocksToRead[currentIndex];
                $log.debug("Read block " + currentBlock.blockId), currentBlock.read = !0, currentBlock.data = result;
                var blockMd5Start = performance.now();
                currentBlock.md5 = arrayBufferUtils.getArrayBufferMd5(currentBlock.data);
                var blockMd5End = performance.now();
                $log.debug("Call to getArrayBufferMd5 for block " + currentBlock.blockId + " took " + (blockMd5End - blockMd5Start) + " milliseconds."), config.state.calculateFileMd5 && config.state.fileMd5.append(currentBlock.data), 0 === currentIndex && doneOne(), currentIndex === numberOfBlocksToRead / 2 - 1 && doneHalf(), ++currentIndex, currentIndex < blocksToRead.length ? readNextBlock() : (config.state.blocksReadIndex = config.state.blocksReadIndex + currentIndex, config.state.readingNextSetOfBlocks = !1, done())
            };
        readNextBlock()
    }

    function blockUploading(block) {
        block.uploading = !0;
        var getReadAndUnprocessed = function () {
            return state.blocks.filter(function (b) {
                return b.read === !0 && b.uploading === !1 && b.resolved === !1
            })
        };
        getReadAndUnprocessed().length < 5 && !state.readingNextSetOfBlocks && readNextSetOfBlocks({
            state: state
        })
    }

    function uploadBlock(block) {
        var deferred = Q.defer();
        if ($log.debug("uploadBlock: block id = " + block.blockId), state.cancelled) return error("cancelled"), deferred.promise;
        var uri = state.blobUri + "&comp=block&blockid=" + block.blockIdBase64,
            requestData = block.data;
        return null === requestData && $log.error("Block " + block.blockId + " has no data to upload!"), blockUploading(block), atomic.put(uri, requestData, {
            "x-ms-blob-type": "BlockBlob",
            "Content-Type": state.file.type,
            "Content-MD5": block.md5.toString(CryptoJS.enc.Base64)
        }).success(function (result, req) {
            $log.debug("Put block successfully " + block.blockId), block.data = null, block.uploading = !1, deferred.resolve({
                requestLength: block.size,
                data: result
            })
        }).error(function (result, req) {
            $log.error("Put block error"), $log.error(data), deferred.error({
                data: result,
                status: req.status
            })
        }), deferred.promise
    }

    function upload() {
        state.blocks = [];
        for (var numberOfBlocks = state.numberOfBlocks, index = 0, totalFileSize = state.fileSize; numberOfBlocks;) {
            var pointer = state.maxBlockSize * (index > 0 ? index : 0),
                end = 0 === index ? state.maxBlockSize : pointer + state.maxBlockSize;
            end > totalFileSize && (end = totalFileSize);
            var blockId = state.blockIdPrefix + pad(index, 6);
            state.blocks.push({
                index: index,
                blockId: blockId,
                blockIdBase64: btoa(blockId),
                pointer: pointer,
                size: end - pointer,
                end: end,
                resolved: !1,
                read: !1,
                data: null,
                md5: null,
                uploading: !1
            }), index++, numberOfBlocks--
        }
        var currentlyProcessing = [],
            addToCurrentlyProcessing = function (block, action) {
                currentlyProcessing.push({
                    action: action,
                    block: block
                })
            },
            removeFromCurrentlyProcessing = function (action) {
                currentlyProcessing.splice(currentlyProcessing.indexOf(_.findWhere(currentlyProcessing, {
                    action: action
                })), 1)
            },
            getUnresolved = function () {
                return state.blocks.filter(function (b) {
                    return b.resolved === !1 && !_.findWhere(currentlyProcessing, {
                        block: b
                    })
                })
            },
            removeProcessedAction = function (action, result) {
                action.resolved = !0, state.bytesUploaded += result.requestLength, state.totalBytesRemaining -= result.requestLength;
                var percentComplete = (parseFloat(state.bytesUploaded) / parseFloat(state.file.size) * 100).toFixed(2);
                self.postMessage({
                    type: "progress",
                    payload: {
                        percentComplete: percentComplete,
                        result: result
                    }
                }), removeFromCurrentlyProcessing(action);
                var hasNext = addNextAction();
                hasNext || _.any(currentlyProcessing) || commitBlockList(state)
            },
            processRejectedAction = function (block, action, rejectReason) {
                removeFromCurrentlyProcessing(action), $log.error(rejectReason)
            },
            addNextAction = function () {
                var unresolved = getUnresolved();
                if (_.any(unresolved)) {
                    var block = unresolved[0],
                        action = uploadBlock(block);
                    action.then(function (result) {
                        block.resolved = !0, removeProcessedAction(action, result)
                    }, function (rejectReason) {
                        block.resolved = !1, processRejectedAction(block, action, rejectReason)
                    }), addToCurrentlyProcessing(block, action)
                }
                return unresolved.length
            };
        state.startedUpload = performance.now(), readNextSetOfBlocks({
            state: state,
            done: function () {
                for (var j = 0; 8 > j; j++) state.blocks[j] && addNextAction(state.blocks[j])
            }
        })
    }

    function commitBlockList() {
        var uri = state.blobUri + "&comp=blocklist",
            requestBody = '<?xml version="1.0" encoding="utf-8"?><BlockList>';
        state.blocks.forEach(function (block) {
            requestBody += "<Latest>" + block.blockIdBase64 + "</Latest>"
        }), requestBody += "</BlockList>", atomic.put(uri, requestBody, {
            "x-ms-blob-content-type": state.file.type
        }).success(function (data, req) {
            self.postMessage({
                type: "complete",
                payload: {
                    data: data,
                    md5: state.fileMd5.finalize().toString(CryptoJS.enc.Base64)
                }
            }), $log.debug("Upload took " + (performance.now() - state.startedUpload) + "ms"), self.close()
        }).error(function (data, req) {
            $log.error("Put block list error " + req.status), $log.error(data), error(data, req.status)
        })
    }

    function pad(number, length) {
        for (var str = "" + number; str.length < length;) str = "0" + str;
        return str
    }

    function importAllScripts(libPath) {
        $log.debug("Agregando libs");
        var addTrailingSlash = function (str) {
                var lastChar = str.substr(-1);
                return "/" !== lastChar && (str += "/"), str
            },
            addLib = function (f) {
                importScripts(addTrailingSlash(libPath) + f)
            };
        addLib("underscore/underscore-min.js"), addLib("cryptojslib/rollups/md5.js"), addLib("cryptojslib/components/lib-typedarrays-min.js"), addLib("cryptojslib/components/enc-base64-min.js"), addLib("atomic/dist/atomic.min.js"), addLib("q/q.js")
    }

    function notifyReady() {
        self.postMessage({
            type: "ready"
        })
    }
    var arrayBufferUtils = function () {
            function arraybuffer2WordArray(arrayBuffer) {
                return CryptoJS.lib.WordArray.create(arrayBuffer)
            }

            function IterativeMd5() {
                this._md5 = null
            }
            return this.getArrayBufferMd5 = function (arrayBuffer) {
                var md5 = CryptoJS.algo.MD5.create(),
                    wordArray = arraybuffer2WordArray(arrayBuffer);
                return md5.update(wordArray), md5.finalize()
            }, IterativeMd5.prototype.append = function (arrayBuffer) {
                null === this._md5 && (this._md5 = CryptoJS.algo.MD5.create());
                var wordArray = arraybuffer2WordArray(arrayBuffer);
                return this._md5.update(wordArray), this
            }, IterativeMd5.prototype.finalize = function () {
                return this._md5.finalize()
            }, this.getArrayBufferMd5Iterative = function () {
                return new IterativeMd5
            }, this
        }(),
        $log = {
            debug: function (message) {
                self.postMessage({
                    type: "log",
                    logType: "debug",
                    message: message
                })
            },
            error: function (message) {
                self.postMessage({
                    type: "log",
                    logType: "error",
                    message: message
                })
            }
        },
        state = {},
        file = null;
    self.onmessage = function (e) {
        switch (e.data.type) {
            case "file":
                setFile(e.data.file);
                break;
            case "config":
                setConfig(e.data.config), importAllScripts(e.data.config.libPath), notifyReady();
                //setConfig(e.data.config), notifyReady();
                break;
            case "upload":
                upload();
                break;
            case "cancel":
                state.cancelled = !0;
                break;
            default:
                throw new Error("Don't know what to do with message of type " + e.data.type)
        }
    }
}();
