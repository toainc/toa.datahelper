'use strict';

/**
 * @ngdoc function
 * @name dbManApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dbManApp
 */
angular.module('dbManApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
