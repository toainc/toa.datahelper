        'use strict';

        /**
         * @ngdoc function
         * @name dbManApp.controller:ComunidadesCtrl
         * @description
         * # ComunidadesCtrl
         * Controller of the dbManApp
         */
        angular.module('dbManApp')
            .controller('ComunidadesCtrl', ['$http', '$scope', 'azureBlobUpload', '$sce', 'BLOB', function ($http, $scope, azureBlobUpload, $sce, blobcfg) {
                var vm = this;
                vm.imgProgress = ";"
                vm.error = "";
                vm.result = "";
                vm.get_comms = {
                    'statements': [
                        {
                            'statement': "MATCH (n:Community)-[r:practices]->(t:Sport) RETURN id(n),n,t.name  ORDER BY n.name"
                        },
                        {
                            'statement': "MATCH (n:Community) WHERE NOT (n)-[:practices]->(:Sport) RETURN id(n),n  ORDER BY n.name"
                        }
                    ]
                };
                vm.comms = {};
                this.getData = function () {
                    $http.defaults.headers.common.Authorization = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
                    $http.defaults.headers.common["Content-Type"] = "application/json";
                    $http.post(url_base + "/transaction/commit", vm.get_comms).
                    success(function (data, status, headers, config) {
                        vm.coms = data.results[0].data;
                        vm.unregcoms = data.results[1].data;
                        console.log(vm.comms);
                    }).
                    error(function (data, status, headers, config) {
                        console.log("error: " + status);
                    });
                }

                this.create = function (community) {

                    $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
                    $http.defaults.headers.common["Content-Type"] = "application/json";
                    $http.post(url_base + "/node/", community.row[1]).
                    success(function (data, status, headers, config) {
                        vm.label(data.metadata.id, function (result) {
                            if (result) {
                                vm.addRel(data.metadata.id, community.sport, function (state) {
                                    if (state) {
                                        vm.result = "Comunidad creada exitosamente :-)";

                                        vm.getData();
                                        vm.doing = false;
                                    } else {
                                        vm.error += "Error al linkear con deporte u.u";
                                        vm.doing = false;
                                    }
                                });
                            } else {
                                vm.error += "Error al crear label :(";
                                vm.doing = false;
                            }
                        });
                    }).
                    error(function (data, status, headers, config) {
                        console.log(data);
                        vm.error += "\nCE: Rayos. Envíale a Guillermo esto... \n" + data.toString();
                        vm.doing = false;

                    });
                };

                this.label = function (id, result) {
                    $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
                    $http.defaults.headers.common["Content-Type"] = "application/json";
                    $http.post(url_base + "/node/" + id + "/labels", "\"Community\"").
                    success(function (data, status, headers, config) {
                        result(true);
                    }).error(function (data, status, headers, config) {
                        console.log(data)
                        vm.error += "\n CL: Rayos. Envíale a Guillermo esto... \n" + data.toString();
                        result(false);
                    });
                }

                this.addRel = function (id, sport, state) {
                    vm.update_rel_cmd = {
                        'statements': [
                            {
                                'statement': "MATCH (n:Community)-[r: practices]->(a:Sport) WHERE id(n)=" + id + " DELETE r"
                                }, {
                                'statement': "MATCH (n:Community), (s:Sport {name:\"" + sport + "\"}) WHERE id(n)=" + id + " CREATE UNIQUE (n)-[r: practices]->(s)"
                        }
                        ]
                    };

                    $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
                    $http.defaults.headers.common["Content-Type"] = "application/json";
                    $http.post(url_base + "/transaction/commit", vm.update_rel_cmd).
                    success(function (data, status, headers, config) {
                        state(true);
                    }).
                    error(function (data, status, headers, config) {
                        console.log("fail en addRel");
                        console.log(data);
                        vm.error += "\n AR: Rayos. Envíale a Guillermo esto... \n" + data;
                        state(false);
                    });
                }

                this.prepare = function (community, isUpdating) {
                    vm.doing = true;
                    vm.imgProgress = "";
                    if (community.imgFile) {
                        blobcfg.blobUri = blobcfg.baseUrl + "community_" + community.row[1].name.replace(" ", "")+".jpg"+"?sv=2014-02-14&sr=c&sig=vWmZQVqRx2fBXtqh6etJa9kSRZJblPOPRMEIp7gsEP0%3D&st=2015-11-07T05%3A00%3A00Z&se=2016-12-01T05%3A00%3A00Z&sp=rw";
                        var url = blobcfg.blobUri;
                        blobcfg.file = community.imgFile;
                        blobcfg.error = function () {
                            vm.error += "Error al subir imagen :(";
                            vm.doing = false;
                        }
                        blobcfg.progress = function(){
                            vm.imgProgress+= ".";
                        }
                        blobcfg.complete = function () {
                            vm.imgProgress = "Imagen subida exitosamente :-)"
                            community.row[1].imgurl = url;
                            if (isUpdating) {
                                vm.update(community);
                            } else {
                                vm.create(community)
                            }
                        }
                        console.log(blobcfg);
                        azureBlobUpload.upload(blobcfg)
                    } else {
                        if (isUpdating) {
                            vm.update(community);
                        } else {
                            vm.create(community)
                        }
                    }
                }

                this.update = function (community) {
                    $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
                    $http.defaults.headers.common["Content-Type"] = "application/json";
                    $http.put(url_base + "/node/" + community.row[0] + "/properties", community.row[1]).
                    success(function (data, status, headers, config) {
                        vm.addRel(community.row[0], community.sport, function (state) {
                            if (state) {
                                vm.result = "Comunidad actualizada exitosamente.";
                                vm.getData();
                                vm.doing = false;
                            } else {
                                vm.error += "\nError al linkear con deporte u.u";
                                vm.doing = false;
                            }
                        });
                    }).
                    error(function (data, status, headers, config) {
                        vm.error += "\n AP: Rayos. Envíale a Guillermo esto... \n" + data;
                        vm.doing = false;
                    });
                };
                this.StoI = function (S) {
                    switch (S) {
                        case "Natación":
                            return 0;
                        case "Crossfit":
                            return 1;
                        case "Running":
                            return 2;
                        case "Triatlón":
                            return 3;
                        case "Fútbol":
                            return 4;
                        case "Ciclismo":
                            return 5;
                        default:
                            return 0;
                    }
                }
                this.process = function (community) {
                    vm.activep = community;
                    vm.activep.sport = community.row[2].toString();
                }

                this._delete = function (place) {
                    var id = place.row[0];
                    vm.isDel = true;
                    var opt = prompt("Por favor escribe \"Sí\" para continuar", "No :(");
                    if (opt != "Sí") {
                        vm.isDel = false;
                        return;
                    }
                    vm._orphan(id, function (res) {
                        if (res) {
                            $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
                            $http.defaults.headers.common["Content-Type"] = "application/json";
                            $http.delete(url_base + "/node/" + id).
                            success(function (data, status, headers, config) {
                                if (status == 204) {
                                    vm.getData();
                                }
                            }).
                            error(function (data, status, headers, config) {
                                vm.error += "\n AP: Rayos. Envíale a Guillermo esto... \n" + data;
                                vm.doing = false;
                            });
                        } else {
                            console.log("fail en orphan");
                        }
                    });
                }
                this._orphan = function (id, res) {
                    vm.orphan_cmd = {
                        'statements': [
                            {
                                'statement': "MATCH (n:Community) OPTIONAL MATCH (n)-[r: practices]->(a:Sport) WHERE id(n)=" + id + " DELETE r"
                                }
                        ]
                    };

                    $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
                    $http.defaults.headers.common["Content-Type"] = "application/json";
                    $http.post(url_base + "/transaction/commit", vm.orphan_cmd).
                    success(function (data, status, headers, config) {
                        res(true);
                    }).
                    error(function (data, status, headers, config) {
                        console.log("fail en orphan");
                        console.log(data);
                        res(false);
                    });
                }

                this.getData();

                $scope.isEmpty = function (string) {
                    return string === "";
                }
                $scope.trustUrl = function (url) {
                    return $sce.trustAsResourceUrl(url);
                }
          }]);
