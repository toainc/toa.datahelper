'use strict';

/**
 * @ngdoc function
 * @name dbManApp.controller:EstablecimientosCtrl
 * @description
 * # EstablecimientosCtl
 * Controller of the dbManApp
 */
angular.module('dbManApp')
    .controller('EstablecimientosCtrl', function ($http, $scope, $sce) {
        var vm = this;
        vm.error = "";
        vm.result = "";
        vm.get_events = {
            'statements': [
                {
                    'statement': "MATCH (n:Place)-[r:COVERS]->(t:Sport) RETURN id(n),n,t.name  ORDER BY n.name"
                },
                {
                    'statement': "MATCH (n:Place) WHERE NOT (n)-[:COVERS]->(:Sport) RETURN id(n),n  ORDER BY n.name"
                }
            ]
        };
        vm.events = {};
        this.getData = function () {
            $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
            $http.defaults.headers.common["Content-Type"] = "application/json";
            $http.post(url_base + "/transaction/commit", vm.get_events).
            success(function (data, status, headers, config) {
                vm.places = data.results[0].data;
                vm.unregplaces = data.results[1].data;
                console.log(vm.events);
            }).
            error(function (data, status, headers, config) {
                console.log("error: " + status);
            });
        }

        this.create = function (event) {
            vm.doing = true;
            console.log(event);
            $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
            $http.defaults.headers.common["Content-Type"] = "application/json";
            $http.post(url_base + "/node/", event.row[1]).
            success(function (data, status, headers, config) {
                vm.label(data.metadata.id, function (result) {
                    if (result) {
                        vm.addRel(data.metadata.id, event.sport, function (state) {
                            if (state) {
                                vm.result = "Establecimiento creado exitosamente :-)";

                                vm.getData();
                                vm.doing = false;
                            } else {
                                vm.error += "Error al linkear con deporte u.u";
                                vm.doing = false;
                            }
                        });
                    } else {
                        vm.error += "Error al crear label :(";
                        vm.doing = false;
                    }
                });
            }).
            error(function (data, status, headers, config) {
                console.log(data);
                vm.error += "\nCE: Rayos. Envíale a Guillermo esto... \n" + data.toString();
                vm.doing = false;

            });
        };

        this.label = function (id, result) {
            $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
            $http.defaults.headers.common["Content-Type"] = "application/json";
            $http.post(url_base + "/node/" + id + "/labels", "\"Place\"").
            success(function (data, status, headers, config) {
                result(true);
            }).error(function (data, status, headers, config) {
                console.log(data)
                vm.error += "\n CL: Rayos. Envíale a Guillermo esto... \n" + data.toString();
                result(false);
            });
        }

        this.addRel = function (id, sport, state) {
            vm.update_rel_cmd = {
                'statements': [
                    {
                        'statement': "MATCH (n:Place)-[r: COVERS]->(a:Sport) WHERE id(n)=" + id + " DELETE r"
                        }, {
                        'statement': "MATCH (n:Place), (s:Sport {name:\"" + sport + "\"}) WHERE id(n)=" + id + " CREATE UNIQUE (n)-[r: COVERS]->(s)"
                }
                ]
            };

            $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
            $http.defaults.headers.common["Content-Type"] = "application/json";
            $http.post(url_base + "/transaction/commit", vm.update_rel_cmd).
            success(function (data, status, headers, config) {
                state(true);
            }).
            error(function (data, status, headers, config) {
                console.log("fail en addRel");
                console.log(data);
                vm.error += "\n AR: Rayos. Envíale a Guillermo esto... \n" + data;
                state(false);
            });
        }


        this.update = function (event) {
            vm.doing = true;
            console.log(event);
            $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
            $http.defaults.headers.common["Content-Type"] = "application/json";
            $http.put(url_base + "/node/" + event.row[0] + "/properties", event.row[1]).
            success(function (data, status, headers, config) {
                vm.addRel(event.row[0], event.sport, function (state) {
                    if (state) {
                        vm.result = "Establecimiento actualizado exitosamente.";
                        vm.getData();
                        vm.doing = false;
                    } else {
                        vm.error += "\nError al linkear con deporte u.u";
                        vm.doing = false;
                    }
                });
            }).
            error(function (data, status, headers, config) {
                vm.error += "\n AP: Rayos. Envíale a Guillermo esto... \n" + data;
                vm.doing = false;
            });
        };
        this.StoI = function (S) {
            switch (S) {
                case "Natación":
                    return 0;
                case "Crossfit":
                    return 1;
                case "Running":
                    return 2;
                case "Triatlón":
                    return 3;
                case "Fútbol":
                    return 4;
                case "Ciclismo":
                    return 5;
                default:
                    return 0;
            }
        }
        this.process = function (event) {
            vm.activep = event;
            vm.activep.sport = event.row[2].toString();
        }

        this._delete = function (place) {
            var id = place.row[0];
            vm.isDel = true;
            var opt = prompt("Por favor escribe \"Sí\" para continuar", "No :(");
            if (opt != "Sí") {
                vm.isDel = false;
                return;
            }
            vm._orphan(id, function (res) {
                if (res) {
                    $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
                    $http.defaults.headers.common["Content-Type"] = "application/json";
                    $http.delete(url_base + "/node/" + id).
                    success(function (data, status, headers, config) {
                        if (status == 204) {
                            vm.getData();
                        }
                    }).
                    error(function (data, status, headers, config) {
                        vm.error += "\n AP: Rayos. Envíale a Guillermo esto... \n" + data;
                        vm.doing = false;
                    });
                } else {
                    console.log("fail en orphan");
                }
            });
        }
        this._orphan = function (id, res) {
            vm.orphan_cmd = {
                'statements': [
                    {
                        'statement': "MATCH (n:Place)-[r: COVERS]->(a:Sport) WHERE id(n)=" + id + " DELETE r"
                        }
                ]
            };

            $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
            $http.defaults.headers.common["Content-Type"] = "application/json";
            $http.post(url_base + "/transaction/commit", vm.orphan_cmd).
            success(function (data, status, headers, config) {
                res(true);
            }).
            error(function (data, status, headers, config) {
                console.log("fail en orphan");
                console.log(data);
                res(false);
            });
        }

        this.getData();





        $scope.isEmpty = function (string) {
            return string === "";
        }
        $scope.trustUrl = function (url) {
            return $sce.trustAsResourceUrl(url);
        }
    });
