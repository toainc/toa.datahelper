'use strict';

/**
 * @ngdoc function
 * @name dbManApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dbManApp
 */
angular.module('dbManApp')
  .controller('MainCtrl', function ($http) {
    var vm = this;
    vm.get_sports = {
            'statements': [
                {
                    'statement': "MATCH (n:Sport) RETURN id(n),n"
                }
            ]
        };
    $http.defaults.headers.common["Authorization"] = "Basic bmVvNGo6RGVwb3J0aXN0YXMxMA==";
            $http.defaults.headers.common["Content-Type"] = "application/json";
            $http.post(url_base + "/transaction/commit", vm.get_sports).
            success(function (data, status, headers, config) {
                vm.Sports = data.results[0].data;
            }).
            error(function (data, status, headers, config) {
                console.log("error: " + status);
            });

  });
