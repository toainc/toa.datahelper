'use strict';

/**
 * @ngdoc function
 * @name dbManApp.controller:MenuctrollerCtrl
 * @description
 * # MenuctrollerCtrl
 * Controller of the dbManApp
 */
angular.module('dbManApp')
  .controller('MenuctrollerCtrl', function ($scope,$location) {
     $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };
  });
