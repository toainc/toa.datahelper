'use strict';

/**
 * @ngdoc directive
 * @name dbManApp.directive:tooltip
 * @description
 * # tooltip
 */
angular.module('dbManApp')
  .directive('tooltip', function () {
    return {restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
  });
