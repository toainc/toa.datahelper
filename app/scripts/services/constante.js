'use strict';

/**
 * @ngdoc service
 * @name dbManApp.constante
 * @description
 * # constante
 * Constant in the dbManApp.
 */
angular.module('dbManApp')
    .constant('BLOB', {
        baseUrl: "https://archivestoa.blob.core.windows.net/app-images/",
        path: '/scripts/',
        libPath: '/scripts/lib/',
        blobUri: "https://archivestoa.blob.core.windows.net/app-images/",
        file: null,
        progress: function cb() {},
        complete: function cb() {},
        error: function cb() {},
        blockSize: 1024, // optional
        calculateFileMd5: true // optional, false by default
    });
