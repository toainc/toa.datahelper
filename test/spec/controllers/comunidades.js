'use strict';

describe('Controller: ComunidadesCtrl', function () {

  // load the controller's module
  beforeEach(module('dbManApp'));

  var ComunidadesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ComunidadesCtrl = $controller('ComunidadesCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ComunidadesCtrl.awesomeThings.length).toBe(3);
  });
});
