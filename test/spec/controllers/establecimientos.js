'use strict';

describe('Controller: EstablecimientosCtrl', function () {

  // load the controller's module
  beforeEach(module('dbManApp'));

  var EstablecimientosCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EstablecimientosCtrl = $controller('EstablecimientosCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(EstablecimientosCtrl.awesomeThings.length).toBe(3);
  });
});
