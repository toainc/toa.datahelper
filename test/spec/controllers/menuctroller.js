'use strict';

describe('Controller: MenuctrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('dbManApp'));

  var MenuctrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MenuctrollerCtrl = $controller('MenuctrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MenuctrollerCtrl.awesomeThings.length).toBe(3);
  });
});
