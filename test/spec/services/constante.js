'use strict';

describe('Service: constante', function () {

  // load the service's module
  beforeEach(module('dbManApp'));

  // instantiate service
  var constante;
  beforeEach(inject(function (_constante_) {
    constante = _constante_;
  }));

  it('should do something', function () {
    expect(!!constante).toBe(true);
  });

});
